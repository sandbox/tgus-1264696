(function ($) {
	
Drupal.behaviors.finance = {

attach: function (context, settings) {
  
  $(window).load(function() {
    update_symbols();
    setInterval(update_symbols, Drupal.settings.finance.update_interval);
  });
  
  var success = function (data) { 
    var symbols = JSON.parse(data);
   
    if(symbols.empty) {
      $("#status").text("Currently there are no symbols added.");
      $("#status").show();
    } else {
      $("#status").hide();
      $("#symbols tbody").empty();
      $("#symbols").show();
    
      $.each(symbols, function(index, E) {  
	var symbol = $("<tr><td>" + E.t + "</td><td>" + E.l_cur + "</td><td>" + E.c + "</td><td>" + E.cp + "</td></tr>");
	$("#symbols tbody").prepend(symbol);
      });
    
    }
  };
  
  var error = function (data) { 
    alert('An error occured updating the stock symbols.');
  };
  
  function update_symbols() {
    $.ajax({ 
      type: 	'GET', 
      url: 	Drupal.settings.basePath + "?q=finance/service",
      dataType:	'json',  
      success: 	success,
      error: 	error 
    }); 
  } 
  
}

};
   
}(jQuery));
