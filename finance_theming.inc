<?php
/**
 * @file
 * Selects a theme passing the settings.
 * NOTE: More themes might be available later which will
 * be selected based on the user's settings.
 */

function theme_finance_theming($var) {
  $settings = $var['settings'];
  return theme('finance_table_theme', array('settings' => $settings));
}

?> 
