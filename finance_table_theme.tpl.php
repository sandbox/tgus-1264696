<?php
  drupal_add_css(drupal_get_path('module', 'finance') . '/finance.css'); 
  drupal_add_js(drupal_get_path('module', 'finance') .'/finance.js');
  drupal_add_js(array('finance' => $settings), 'setting');
?>

<div id="status"></div>

<table id="symbols" cellspacing="0">
  <thead>
  <tr>
    <th>Symbol</th>
    <th>Price</th>
    <th>Change</th>
    <th>%</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>
