<?php

/**
 * @file
 * Theme, permission, menu, and block settings.
 */

// Implements hook_help
function finance_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#finance':
      $output = '<p>' . t('Track the development of this module at <a href="http://github.com/tgus">github</a>.') . '</p>';
      break;
  }
  return $output;
}

// Implements hook_permission
function finance_permission() {
  return array(
    'administer stock symbols' => array(
      'title' => t('Administer Stock Symbols'),
      'description' => t('Permission to add and remove stock symbols and modify the settings.'),
    )
  );
}

// Implements hook_menu
function finance_menu() {
  $items = array();

  $items['finance'] = array( 
    'title' => 'Finance', 
    'page callback' => 'finance_block_view', 
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM, 
  ); 

  $items['admin/config/services/finance'] = array(
    'title'            => 'Finance',
    'description'      => 'Add or remove stock symbols and set the update interval.',
    'page callback'    => 'manage_symbols',  
    'access arguments' => array('administer stock symbols'),
    'file'             => 'manage.forms.inc',
  );

  $items['admin/config/services/finance/list'] = array(
    'title'            => 'Manage Symbols',
    'page callback'    => 'drupal_get_form',
    'access arguments' => array('administer stock symbols'),
    'type'             => MENU_DEFAULT_LOCAL_TASK,
    'file'             => 'manage.forms.inc',  
  );

  $items['admin/config/services/finance/settings'] = array(
    'title'            => 'Settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('finance_settings_form'),
    'access arguments' => array('administer stock symbols'),
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'settings.forms.inc',
  );

  $items['admin/config/services/finance/delete/%'] = array(
    'title'            => 'Delete symbol',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('symbol_delete_confirm', 5),
    'access arguments' => array('administer stock symbols'),
    'type'             => MENU_CALLBACK,
    'file'             => 'manage.forms.inc',
  );

  $items['finance/service'] = array( 
    'title' => 'Stock symbol service', 
    'page callback' => 'service_json',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK, 
  ); 
  
  return $items;
}


function service_json() {

  drupal_add_http_header('Content-Type', 'application/json');

  $records = db_query("SELECT symbol FROM {finance} ORDER BY added DESC");

  if($records->rowCount() == 0) {
    drupal_json_output('{ "empty" : true }');
    exit();
  } 

  $symbol_set = array();
  foreach($records as $record) {
    $symbol_set[] = $record->symbol;
  }

  // Creates a comma separated list of symbols for the url request
  $symbols_url = implode(",", $symbol_set); 

  $request = check_url('http://www.google.com/finance/info?q=' . urlencode($symbols_url));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $request);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($curl, CURLOPT_TIMEOUT, 10);
  $data = curl_exec($curl);
  $info = curl_getinfo($curl);

  if ($info['http_code'] != '200') {
    form_set_error('', t('There was a problem connecting to Google.'));
    return false;
  }

  $symbols = substr($data, 3); // remove commented json reply

  drupal_json_output($symbols);

}

// Implements hook_block_info
function finance_block_info() {

  $blocks['finance_block'] = array(
    'info' => t('Manage stock symbols.'),
    'cache' => DRUPAL_NO_CACHE,
    'region' => 'content',
    'weight' => 0, 
    'status' => 1,
    'pages' => 'finance',
    'visibility' => 1
  );

  return $blocks;
}


function finance_settings() {
  $settings = db_query('SELECT update_interval FROM {finance_settings} WHERE id=1')->fetchObject();

  // If there are no default settings, create them
  if (empty($settings)) {
    $settings = new stdClass;
    $settings->update_interval = 15000;
  } 

  return $settings;
}

// Implements hook_theme
function finance_theme() {
  return array(
    'finance_theming' => array(
      'file' => 'finance_theming.inc',
      'variables' => array('settings' => null)
    ),
    'finance_table_theme' => array(
      'template' => 'finance_table_theme',
      'variables' => array('settings' => null)
    )
  );  
}

// Implements hook_block_view
function finance_block_view() {
  $block['subject'] = t('Stock Symbol Data');
  $block['content'] = '';        
  $settings = finance_settings();
  $block['content'] .= theme('finance_theming',array('settings' => $settings));

  return $block;
}

?>
