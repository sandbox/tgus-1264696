<?php

/**
 * @file
 * Form for stock symbol settings.
 */

function finance_settings_form() {
  
  $form['finance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  
  $form['finance']['seconds'] = array(
    '#type' => 'textfield',
    '#title' => t('Polling interval:'),
    '#description' => t('Determines how often sybmols update. This interval must be between 15 and 300 seconds.'),
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

// Implements hook_form_validate
function finance_settings_form_validate($form, $form_state) {
  $seconds = (int)trim(check_plain($form_state['values']['seconds']));

  if($seconds < 15 || $seconds > 300) {
    form_set_error('Update Interval', t('The update interval must be between 15 and 300 seconds.'));
    return false;
  }
  
  return true;
}

// Implements hook_form_submit
function finance_settings_form_submit($form, $form_state) {
  $seconds = (int)trim(check_plain($form_state['values']['seconds']));

  $millisec = $seconds * 1000;
  $query = db_query('UPDATE {finance_settings} SET update_interval = :update_interval WHERE id=1',array(':update_interval' => $millisec));

  if ($query == false) {
    drupal_set_message(t('There was a problem with the database connection.'));
  } else {
    drupal_set_message(t('Update interval successfully updated.'));
  }
}

?> 
