<?php
/**
 * @file
 * Form for managing stock symbols.
 */

function add_symbol_form() {
  $form['finance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a new stock symbol'),
  );
  
  $form['finance']['symbol'] = array(
    '#type' => 'textfield',
    '#title' => t('Symbol name:'),
    '#description' => t('eg. GOOG'),
    '#size' => 10,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

// Implements hook_form_validate
function add_symbol_form_validate($form, $form_state) {
  $symbol = trim(check_plain($form_state['values']['symbol']));

  $result = db_query("SELECT id FROM {finance} WHERE symbol = :symbol", array(':symbol' => $symbol));

  if ($result->fetch() == true) {
    form_set_error('', t('Symbol already exists in the database.'));
  } else {
    $request = check_url('http://www.google.com/finance/info?q=' . $symbol);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $request);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_exec($curl);
    $info = curl_getinfo($curl);

    if ($info['http_code'] != '200') {
      form_set_error('', t('The symbol you entered does not appear to be a valid stock symbol.'));
      return false;
    }
  }

}

// Implements hook_form_submit
function add_symbol_form_submit($form, $form_state) {
  $symbol = trim(check_plain($form_state['values']['symbol']));

  $query = db_insert('finance')->fields(array('symbol' => $symbol, 'added' => REQUEST_TIME));

  if ($query->execute()) {
    drupal_set_message(t('Symbol successfully added.'));
  } else {
    drupal_set_message(t('There was a problem with the database connection.'));
  }

}

function manage_symbols() {
  $form = array();
  $form['add_symbol_form'] = drupal_get_form('add_symbol_form');
  $form['list_symbols_form'] = drupal_get_form('list_symbols_form');
  return $form;
}

function list_symbols_form() {
  $rows = array();
  $records = db_query("SELECT * FROM {finance} ORDER BY added DESC");
  $count = $records->rowCount();
  $empty = '';

  $header = array(t('Symbol'), t('Remove symbol'));
  $form['content']['#theme'] = 'table';

  if($count == 0) {
    $empty = "Currently there are no symbols added.";
    $header = array();
  }
  
  foreach($records as $record) {
    $rows[$record->id] = array(($record->symbol));
    $rows[$record->id][] = l(t('Remove'), 'admin/config/services/finance/delete/' . $record->id);
  }

  $form['content']['#header'] = $header;
  $form['content']['#rows'] = $rows;
  $form['#attached'] = array('css' => array(drupal_get_path('module', 'finance') . '/finance.css'));
  $form['content']['#empty'] = $empty;

  return $form;
}

function finance_load_symbol($id) {
  $query = db_query("SELECT symbol FROM {finance} WHERE id = :id", array(':id' => $id));
  
  if ($query->rowCount() == 1) {
    $symbol = $query->fetch();
    return array('symbol' => $symbol, 'is_found' => true);
  }
  return array('is_found' => false);
}

function symbol_delete_confirm($form, $form_state, $id) {
  $symbol = finance_load_symbol($id);
  if ($symbol['is_found'] == true) {
    $form = array();
    $form['id'] = array('#type' => 'hidden', '#value' => $id);

    $question = t('Are you sure you want to remove %id?', array('%id' => $symbol['symbol']->symbol));
    $desc = t('If deleted, this stock symbol will no longer be tracked.');
    $remove = t('Remove');
    return confirm_form($form, $question, 'admin/config/services/finance/', $desc, $remove);
  } else {
    drupal_set_message('No such symbol found', 'error');
    drupal_goto('admin/config/services/finance/');
  }
}

// Implements hook_form_submit
function symbol_delete_confirm_submit($form, $form_state) {
  $query = db_delete('finance')->condition('id', $form_state['values']['id'])->execute();
  drupal_set_message(t('The symbol was deleted'));
  drupal_goto('admin/config/services/finance/');
}

?>
