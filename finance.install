<?php

/**
 * @file
 * Database installation schema and uninstallation.
 */

// Implements hook_schema
function finance_schema() {
  $schema['finance'] = array(
    'description' => 'The stock symbols',
    'fields' => array(
      'id' => array(
	'description' => 'The unique identifier for a symbol',
	'type' => 'serial',
	'unsigned' => TRUE,
	'not null' => TRUE,
      ),
      'symbol' => array(
	'description' => 'The symbol name',
	'type' => 'varchar',
	'length' => 8,
	'not null' => TRUE,
	'default' => '',
      ),
      'added' => array(
	'description' => 'Timestamp when the symbol was added',
	'type' => 'int',
	'length' => 12,
	'not null' => TRUE,
	'default' => 0,
      ),
    ),
    'indexes' => array('added' => array('added')),
    'primary key' => array('id'),
  );

  $schema['finance_settings'] = array(
    'description' => 'The stock symbols settings table',
    'fields' => array(
      'id' => array(
        'description' => 'The unique identifier for the settings',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'update_interval' => array(
        'description' => 'How often the symbols will update in milliseconds.',
        'type' => 'int', 'not null' => TRUE, 'default' => 15000
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

// Implements hook_uninstall
function finance_uninstall() {
  drupal_uninstall_schema('finance');
  drupal_uninstall_schema('finance_settings');
}

?>
